package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/thoj/go-ircevent"
	"gitlab.com/Niesch/Juliav2/bestäm"
	"gitlab.com/Niesch/Juliav2/butkus"
	"gitlab.com/Niesch/Juliav2/lotto"
	"gitlab.com/Niesch/Juliav2/väder"
	"math/rand"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"
)

type ServerList struct {
	Servers           []*Server
	SimpleResponders  []*SimpleResponder
	NamedResponders   []*NamedResponder
	CombineResponders []*CombineResponder
}

type Server struct {
	Address     string
	SSL         bool
	InsecureSSL bool
	EnableTitle bool
	Channels    []string
	Realname    string
	Nickname    string
	Prefix      string
	Version     string
	Ignores     []string
}

type SimpleResponder struct {
	Trigger     string
	Responses   []string
	MessageType string
}

type CombineResponder struct {
	Trigger     string
	Components  [][]string
	UserIndex   int
	MessageType string
}

type NamedResponder struct {
	Trigger     string
	MessageType string
	Responses   []string
}

func main() {
	path := flag.String("config", "JuliaV2.json", "path to the json configuration file")
	flag.Parse()
	f, err := os.Open(*path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	d := json.NewDecoder(f)
	sl := ServerList{}
	wg := sync.WaitGroup{}

	err = d.Decode(&sl)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	for _, server := range sl.Servers {
		wg.Add(1)
		conn := irc.IRC(server.Nickname, server.Realname)
		conn.Version = server.Version

		if server.SSL {
			conn.UseTLS = true
			if server.InsecureSSL {
				conn.TLSConfig = &tls.Config{InsecureSkipVerify: true}
			}
		}

		// Copy configuration into local variables because raceyness
		channels := server.Channels
		address := server.Address
		enabletitle := server.EnableTitle
		ignores := server.Ignores

		err = conn.Connect(address)
		if err != nil {
			fmt.Println(err)
		}

		conn.AddCallback("001", func(e *irc.Event) {
			for _, channel := range channels {
				fmt.Printf("Entering channel %s on %s\n", channel, address)
				conn.Join(channel)
			}
		})
		reg := regexp.MustCompile("(?i)(https?:\\/\\/)([a-z\\-0-9\\._]+\\.[a-z]{2,3}[a-z0-9%\\-\\._?#&\\/=]*)")
		conn.AddCallback("PRIVMSG", func(e *irc.Event) {
			go func(e *irc.Event) {
				m := e.Message()

				for _, nick := range ignores {
					if strings.EqualFold(nick, e.Nick) {
						return
					}
				}

				if enabletitle {
					// check if it matches a url regex
					urlMatch := reg.FindStringSubmatch(m)
					if len(urlMatch) != 0 {
						title, err := getTitle(urlMatch[0])
						if err != nil || title == "" {
							return
						}

						if !strings.HasPrefix(title, "Attention Required!") || !strings.Contains(title, "Cloudflare") {
							conn.Privmsg(e.Arguments[0], fmt.Sprintf("[Title] %s", title))
						}
					}
				}
				// Just because you matched a URL doesn't mean you're done.
				// Don't handle messages that don't satisfy the prefix.
				if !strings.HasPrefix(m, server.Prefix) {
					return
				}

				// Strip the message of its prefix.
				m = strings.TrimPrefix(m, server.Prefix)

				// Check simple responders first
				for _, resp := range sl.SimpleResponders {
					if strings.Split(m, " ")[0] == resp.Trigger {
						if resp.MessageType == "PRIVMSG" {
							conn.Privmsg(e.Arguments[0], resp.Responses[r.Intn(len(resp.Responses))])
						} else if resp.MessageType == "ACTION" {
							conn.Action(e.Arguments[0], resp.Responses[r.Intn(len(resp.Responses))])
						}
						return
					}
				}

				// Check if we're matching a Named Responder
				for _, nresp := range sl.NamedResponders {
					if strings.Split(m, " ")[0] == nresp.Trigger {
						target := strings.TrimSpace(strings.TrimPrefix(m, nresp.Trigger))
						if target == "" {
							conn.Privmsg(e.Arguments[0], fmt.Sprintf("%s behöver argument.", nresp.Trigger))
							return
						}

						if nresp.MessageType == "PRIVMSG" {
							conn.Privmsg(e.Arguments[0], fmt.Sprintf(nresp.Responses[r.Intn(len(nresp.Responses))], target))
						} else if nresp.MessageType == "ACTION" {
							conn.Action(e.Arguments[0], fmt.Sprintf(nresp.Responses[r.Intn(len(nresp.Responses))], target))
						}

						return
					}
				}

				// Check if we're matching a CombineResponder
				for _, cresp := range sl.CombineResponders {
					if strings.Split(m, " ")[0] == cresp.Trigger {
						target := strings.TrimSpace(strings.TrimPrefix(m, cresp.Trigger))
						if cresp.UserIndex >= 0 && target == "" {
							conn.Privmsg(e.Arguments[0], fmt.Sprintf("%s behöver argument.", cresp.Trigger))
							return
						}

						var s string

						for i, arr := range cresp.Components {
							if i == cresp.UserIndex {
								s += target + " "
							}
							s += arr[r.Intn(len(arr))] + " "
						}

						if cresp.MessageType == "PRIVMSG" {
							conn.Privmsg(e.Arguments[0], s)
						} else if cresp.MessageType == "ACTION" {
							conn.Action(e.Arguments[0], s)
						}

						return
					}
				}

				// We failed to find a responder for this message. This is where features needing actual code go.
				switch strings.Split(m, " ")[0] {
				case "lotto":
					conn.Privmsg(e.Arguments[0], lotto.GetLine())
				case "butkus":
					conn.Privmsg(e.Arguments[0], butkus.GetLine())
				case "bestäm":
					conn.Privmsg(e.Arguments[0], bestäm.Bestäm(strings.Split(strings.TrimSpace(strings.TrimPrefix(m, "bestäm")), " ")))
				case "väder":
					ws, err := väder.GetWeatherByName(strings.TrimPrefix(m, "väder "))
					if err == nil {
						conn.Privmsg(e.Arguments[0], ws)
					}
				case "title":
					enabletitle = !enabletitle
					conn.Privmsg(e.Arguments[0], fmt.Sprintf("Title-fetching: %t", enabletitle))
				}
				return
			}(e)
		})
		go conn.Loop()
	}
	wg.Wait() // We'll never return.
}
