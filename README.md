# JuliaBot den Andra
Vår älskade JuliaBot i en ny tappning.


## Mål med omskrivning:
- Förenkla för samarbete
- Stöd för multipla servrar från en binär

## Funktioner från JuliaBot:
- [X] butkus
- [X] hellseger
- [X] räf
- [X] ss
- [X] älska
- [X] kattljud
- [X] mörda
- [X] knarka
- [X] vape
- [X] bestäm (Förbättrad sedan JuliaBot v1)
- [X] diagnos
- [X] lotto (Förbättrad sedan JuliaBot v1)

## TODO:
- [X] URL-parsing (<title> grabbing)
