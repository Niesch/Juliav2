package butkus

import (
	"io/ioutil"
	"net/http"
)

func GetLine() string {
	resp, err := http.Get("https://butkus.xyz/api/quote")
	if err != nil {
		return err.Error()
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err.Error()
	}
	return string(body)
}
